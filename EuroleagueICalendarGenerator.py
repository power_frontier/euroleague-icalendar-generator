# importing required modules 
import PyPDF2
from icalendar import Calendar, Event
from datetime import datetime
from datetime import timedelta
import pytz
from pytz import timezone
import socket
import os
import Defaults 
import configargparse

# parsing arguments
p = configargparse.ArgParser(default_config_files=['./EuroleagueICalendarGenerator.conf'])

p.add('-i', '--input-file', required=True, help='path to the pdf calendar file to process')  # this option can be set in a config file because it starts with '--'
p.add('-o', '--output-file', help='path to the file to output the calendar in ical format. If not specified outputs the content on stdout')
p.add('-t', '--team', required=True, help='team to generate his calendar')
p.add('-pc', '--prev-cols', default=Defaults.PREV_COLS, help='number of columns of the pdf table minus one')
p.add('-tz', '--time-zone', default=Defaults.TIME_ZONE, help='time zone to convert dates of the matches. If not specified gets one from CET (Europe/Madrid). Time zones could be found on: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones')
p.add('-et', '--event-title', default=Defaults.EVENT_TITLE, help='prefix of the events titles generated')
p.add('-d', '--event-duration', default=Defaults.EVENT_DURATION, help='duration of the events generated')
p.add('-lf', '--local-first', action='store_true', default=Defaults.LOCAL_FIRST, help='flag that if present or true indicates that the team would be placed first on the events titles generated if they plays as local team')
p.add('-c', '--my-config', is_config_file=True, help='config file path')
p.add('-v', help='verbose', action='store_true')
#p.add('vcf', nargs='+', help='variant file(s)')



options = p.parse_args()

print(options)
print("----------")
print(p.format_help())
print("----------")
print(p.format_values())    # useful for logging where different settings came from
print("----------")

prev_cols = int(options.prev_cols)

	
# creating a pdf file object 
pdfFileObj = open(options.input_file, 'rb') 
 
# creating a pdf reader object 
pdfReader = PyPDF2.PdfFileReader(pdfFileObj) 
 
cal = Calendar()

# calendar header
pageObj = pdfReader.getPage(0)
text = pageObj.extractText()
cal['dtstart'] = datetime.now().strftime("%Y%m%dT%H%M%S") #'20050404T080000'
pos = text.find(":")
year = text[pos-7:pos-3]
season = year+'-'+str(int(year[2:4])+1)
summary = season+' Euroleague Basketball Calendar for '+options.team+' team'
cal['summary'] = summary
cal['prodid'] = "-//EuroleagueICalendarGenerator/NONSGML EuroleagueICalendarGenerator V1.0//EN"
cal['version'] = "2.0"

hostname = socket.gethostname()
pid = os.getpid()
matchDurationObj = timedelta(hours=int(options.event_duration))
nEvent = 1

# calendar Events
count = pdfReader.numPages

for i in range(count):
	
	# creating a page object 
	pageObj = pdfReader.getPage(i)
	# extracting text from page 
	text = pageObj.extractText()
	
	beg = text.find(options.team)
	prev_beg = 0
	
	while beg!=-1: 
		aux = text[prev_beg:beg-1]
		prevLines = aux.rsplit("\n", prev_cols) # len(prevLines) -> 5
		
		time = prevLines[prev_cols]
		if time.find(":") == -1: 
			opponent = time
			localTeam = 0
			time = prevLines[prev_cols-1]
			date = prevLines[prev_cols-3]
		else:
			aux = text[beg:]
			nextLines = aux.split("\n", 2)
			opponent = nextLines[1]
			localTeam = 1
			date = prevLines[prev_cols-2]
			
		aux = date.split(", ")
		if aux[1][0].isdigit():
			monthPattern = "%d %B" # Thursday, 1 November 2018
		else:
			monthPattern = "%B %d" # Thursday, October 11, 2018
		if len(aux) == 3:
			monthPattern = monthPattern+"," # October 11, 2018 VS 1 November 2018 <- A comma before the year or not
		
		datetimeObj = datetime.strptime(date+time, '%A, '+monthPattern+' %Y%H:%M')
		utc = pytz.utc
		utc_datetimeObj = datetimeObj.replace(tzinfo=utc)
		dest_tz = timezone(options.time_zone)
		dest_datetimeObj = utc_datetimeObj.astimezone(dest_tz)
		
		event = Event()
		generation_time = datetime.now().strftime("%Y%m%dT%H%M%S")
		event['uid'] = str(nEvent)+'-'+generation_time+'-'+str(pid)+'@'+hostname #generating a almost unique identifier for every event
		event['dtstamp'] = generation_time
		event['dtstart'] = dest_datetimeObj.strftime("%Y%m%dT%H%M%S")
		event['dtend'] = (dest_datetimeObj+matchDurationObj).strftime("%Y%m%dT%H%M%S")
		if (localTeam==1 and options.local_first) or (localTeam==0 and not(options.local_first)) :
			event['summary'] = options.event_title+options.team+" VS "+opponent
		else:
			event['summary'] = options.event_title+opponent+" VS "+options.team
		cal.add_component(event)
		
		nEvent=nEvent+1
		prev_beg = beg
		beg = text.find(options.team, beg+1)
		

def display(cal):
	return (cal.to_ical().replace(b'\r\n', b'\n').strip())

if options.output_file == None :
	print (display(cal).decode())

# closing the pdf file object 
pdfFileObj.close()

